package com.pig.demos.designPatterns.responsibilityChain;

/**
 * created by MyPC on 2020/7/22
 */
public class ErrorLogger extends AbstractLogger {

    public ErrorLogger(int level) {
        this.level = level;
    }

    @Override
    protected void write(String message) {
        System.out.println("ErrorLogger: " + message);
    }
}