package com.pig.demos.mybatisDemo;

import com.pig.demos.mybatisDemo.GetModel;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class myselfBatis {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        String url = "jdbc:mysql://localhost:3306/hubin?autoReconnect=true&useSSL=false&useUnicode=true&characterEncoding=utf-8&zeroDateTimeBehavior=convertToNull&serverTimezone=GMT%2B8";
        String user = "root";
        String pass = "12345678";
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection conn  =  DriverManager.getConnection(url,  user,  pass);
        //java文件输出路径
        String path = System.getProperty("user.dir");
        GetModel getModel  = new GetModel(conn);
        /*
        使用：
            pojo: 包含属性与getter、setter
            bean: 包含属性与getter、setter与无参构造器，并实现Serializable接口
         */
        //生成pojo到path
//        getModel.generatePojo(path);
        //生成pojo到path，在pojo中添加包信息"package com.ren.model"
        getModel.generatePojo(path, "com.pig.demos.model");
        //生成Bean到path
        //getModel.generateBean(path);
        //生成Bean到path，在Bean中添加包信息"package com.ren.bean"
        //getModel.generateBean(path, "com.ren.bean");
    }
}
