package com.pig.demos.controller;

import com.pig.demos.spring.B;
import com.pig.demos.spring.MyLifeCycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.awt.print.Book;
import java.util.Map;

@RestController
public class TestController {
    private int a = 0;
    @Autowired
    private Map mySelfCache;
    @Autowired
    private MyLifeCycle MyLifeCycle;

    @GetMapping("/hello")
    public String testHello() {
        System.out.println(a++);
        return "hello";
    }

    @GetMapping("/getCache1")
    public Object getCache1() {
        mySelfCache.put("test1", "test1");
        return mySelfCache;
    }

    @GetMapping("/adviceTest")
    public Object adviceTest() {
        return 1 / 0;
    }
}

