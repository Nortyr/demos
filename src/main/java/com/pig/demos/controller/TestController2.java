package com.pig.demos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class TestController2 {

    @Autowired
    private Map mySelfCache;

    @GetMapping("/getCache2")
    public Object getCache2() {
        mySelfCache.put("test2", "test2");
        return mySelfCache;
    }
}
