package com.pig.demos.requestResponse;

import com.sun.deploy.net.HttpResponse;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class RequestResponseDemo {
    @GetMapping("testRequest")
    public String testRequest(@RequestParam String a, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        System.out.println("------------------------------------------------------------------");
        System.out.println("getAuthType..." + httpRequest.getAuthType());
        System.out.println("getCookies..." + httpRequest.getCookies());
        System.out.println("getDateHeader..." + httpRequest.getDateHeader("method"));
        System.out.println("getHeaderNames..." + httpRequest.getHeaderNames());
        System.out.println("getMethod..." + httpRequest.getMethod());
        System.out.println("getPathInfo..." + httpRequest.getPathInfo());
        System.out.println("getPathTranslated..." + httpRequest.getPathTranslated());
        System.out.println("getContextPath..." + httpRequest.getContextPath());
        System.out.println("getQueryString..." + httpRequest.getQueryString());
        System.out.println("getRemoteUser..." + httpRequest.getRemoteUser());
        System.out.println("getUserPrincipal..." + httpRequest.getUserPrincipal());
        System.out.println("getRequestedSessionId..." + httpRequest.getRequestedSessionId());
        System.out.println("getRequestURI..." + httpRequest.getRequestURI());
        System.out.println("getRequestURL..." + httpRequest.getRequestURL());
        System.out.println("getServletPath..." + httpRequest.getServletPath());
        System.out.println("getSession..." + httpRequest.getSession());
        System.out.println("changeSessionId..." + httpRequest.changeSessionId());
        System.out.println("isRequestedSessionIdValid..." + httpRequest.isRequestedSessionIdValid());
        System.out.println("isRequestedSessionIdFromCookie..." + httpRequest.isRequestedSessionIdFromCookie());
        System.out.println("isRequestedSessionIdFromURL..." + httpRequest.isRequestedSessionIdFromURL());
        System.out.println("getSession..." + httpRequest.getSession());
        System.out.println("--------------------------------------------------------------");


        return "a";
    }
}
