package com.pig.demos.sync;

import java.util.concurrent.CountDownLatch;

/**
 * created by MyPC on 2020/7/28
 */
public class CountDownLatchDemo {
    public static void main(String[] args) throws InterruptedException {
        Thread[] threads = new Thread[100];
        CountDownLatch latch = new CountDownLatch(threads.length);
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Thread(() -> {
                int count = 0;
                for (int j = 0; j < 10; j++) {
                    synchronized (CountDownLatchDemo.class) {
                        count++;
                        System.out.println(count);
                        latch.countDown();
                        System.out.println("latch" + latch.getCount());
                    }


                }
            });
        }
        for (int i = 0; i < threads.length; i++) {
            threads[i].start();
        }
        //latch-成0了，就能继续往下执行
        latch.await();
        System.out.println("---------end");

    }
}
