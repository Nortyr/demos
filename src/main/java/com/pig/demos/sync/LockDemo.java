package com.pig.demos.sync;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * created by MyPC on 2020/7/28
 */
public class LockDemo {


    public static void main(String[] args) throws InterruptedException {


    }

    /**
     * tryLock没有加锁
     */
    private static void tryLockDemo() {
        Lock lock = new ReentrantLock();
        Thread thread = new Thread(() -> {
            try {
                lock.lock();
                System.out.println("t1 start");
                Thread.sleep(10000);
                System.out.println("t1 end");
            } catch (Exception e) {
                System.out.println("t1 interrupt");
            } finally {
                lock.unlock();
            }
        });
        Thread thread1 = new Thread(() -> {
            try {
                boolean a = lock.tryLock(1, TimeUnit.SECONDS);
                System.out.println("t2 start" + a);
                Thread.sleep(100);
                System.out.println("t2 end");
            } catch (Exception e) {
                System.out.println("t2 interrupt");

            } finally {
                lock.unlock();
            }
        });
        thread.start();
        thread1.start();
    }

    /**
     * 底层cas
     */
    /**
     * 线程被打断
     *
     * @throws InterruptedException
     */
    private static void lockInterrupt() throws InterruptedException {
        Lock lock = new ReentrantLock();
        Thread thread = new Thread(() -> {
            try {
                lock.lockInterruptibly();
                System.out.println("t1 start");
                Thread.sleep(Integer.MAX_VALUE);
                System.out.println("t1 end");
            } catch (Exception e) {
                System.out.println("t1 interrupt");
            } finally {
                lock.unlock();
            }
        });
        Thread thread1 = new Thread(() -> {
            try {
                lock.lockInterruptibly();
                System.out.println("t2 start");
                Thread.sleep(100);
                System.out.println("t2 end");
            } catch (Exception e) {
                System.out.println("t2 interrupt");

            } finally {
                lock.unlock();
            }
        });
        thread.start();
        thread1.start();
        Thread.sleep(10);
        thread.interrupt();
    }
}
