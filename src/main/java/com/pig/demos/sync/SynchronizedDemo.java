package com.pig.demos.sync;

/**
 * created by MyPC on 2020/7/26
 */
public class SynchronizedDemo {
    public synchronized void test1() throws InterruptedException {
        System.out.println("test1");

        Thread.sleep(10000);
        System.out.println("test1 end");
        test3();
    }

    public static synchronized void test2() throws InterruptedException {
        System.out.println("test2");
        Thread.sleep(10000);
        System.out.println("test2 end");

    }

    public void test3() throws InterruptedException {
        synchronized (this) {
            System.out.println("test3");
            Thread.sleep(10000);
            System.out.println("test3 end");
        }
    }

    public void test4() throws InterruptedException {
        synchronized (SynchronizedDemo.class) {
            System.out.println("test4");
            Thread.sleep(10000);
            System.out.println("test4 end");
            test2();
        }
    }

    public static void main(String[] args) {
        SynchronizedDemo synchronizedDemo = new SynchronizedDemo();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                try {
//                    synchronizedDemo.test1();
                    synchronizedDemo.test2();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Runnable r1 = new Runnable() {
            @Override
            public void run() {
                try {
//                    synchronizedDemo.test3();
                    synchronizedDemo.test4();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread t1 = new Thread(r);
        Thread t2 = new Thread(r1);
//        t1.start();
        t2.start();
    }
}
