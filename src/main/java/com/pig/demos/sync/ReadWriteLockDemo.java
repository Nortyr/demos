package com.pig.demos.sync;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * created by MyPC on 2020/7/28
 */
public class ReadWriteLockDemo {
    static ReadWriteLock lock = new ReentrantReadWriteLock();
    static Lock lock1 = lock.readLock();
    static Lock lock2 = lock.writeLock();
    static int value = 1;

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 18; i++) {
            new Thread(() -> {
                try {
                    ReadWriteLockDemo.read(lock1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }
        for (int i = 0; i < 18; i++) {
            new Thread(() -> {
                try {
                    ReadWriteLockDemo.write(lock2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }

    }

    public static void read(Lock lock) throws InterruptedException {
        try {
            lock.lock();
            Thread.sleep(1000);
            System.out.println("read over ..." + value);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public static void write(Lock lock) throws InterruptedException {
        try {
            lock.lock();
            Thread.sleep(1000);
            value = 2;
            System.out.println("write over");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}
