package com.pig.demos.sync;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * created by MyPC on 2020/7/28
 */
public class LockSupportDemo {
    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                System.out.println(i);
                if (i == 5) {
                    //当前线程阻塞
                    LockSupport.park();
                }
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
        //unpark可以先于park调用
        LockSupport.unpark(t);

//        TimeUnit.SECONDS.sleep(8);
//        System.out.println("8s out");
//        LockSupport.unpark(t);
    }
}
