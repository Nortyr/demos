package com.pig.demos.sync;

import java.util.concurrent.Exchanger;

/**
 * created by MyPC on 2020/7/28
 */
public class ExchangerDemo {
    public static void main(String[] args) {
        Exchanger<String> exchanger = new Exchanger();
        new Thread(() -> {
            String s = "T1";
            try {
                s = exchanger.exchange(s);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " " + s);
        }, "T1").start();
        new Thread(() -> {
            String s = "T2";
            try {
                //需要等到某个线程交换完
                Thread.sleep(10000);
                s = exchanger.exchange(s);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " " + s);
        }, "T2").start();
    }
}
