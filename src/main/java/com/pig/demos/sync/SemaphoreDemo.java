package com.pig.demos.sync;

import java.util.concurrent.Semaphore;

/**
 * created by MyPC on 2020/7/28
 */
public class SemaphoreDemo {
    public static void main(String[] args) {
        Semaphore s = new Semaphore(2);//允许多少个线程同时执行
        //取一下，这个值变成0，变成0别的线程就不能访问
        new Thread(() -> {
            try {
                s.acquire();
                System.out.println("t1 running");
                Thread.sleep(200);
                System.out.println("t1 running");
                s.release();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                s.acquire();
                System.out.println("t2 running");
                Thread.sleep(200);
                System.out.println("t2 running");
                s.release();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
