package com.pig.demos.sync;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * 栅栏，一批一批放行
 * created by MyPC on 2020/7/28
 */
public class CyclicBarrierDemo {
    public static void main(String[] args) {
//        CyclicBarrier barrier=new CyclicBarrier(20);
        CyclicBarrier barrier = new CyclicBarrier(20, new Runnable() {
            @Override
            public void run() {
                System.out.println("----------------overd 20");
            }
        });
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                try {
                    //线程到这里停住，满制定个数后，一起执行
                    barrier.await();
                    System.out.println("aaa");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }
}
