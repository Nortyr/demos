package com.pig.demos.threadPool;

import com.sun.jmx.snmp.tasks.ThreadService;

import java.util.concurrent.*;

/**
 * 最新的demo
 */
public class ThreadPoolDemo2 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService threadService=new ThreadPoolExecutor(5,
                10,
                50,TimeUnit.MILLISECONDS,
                new ArrayBlockingQueue<Runnable>(5),//等待队列
                Executors.defaultThreadFactory(),//线程创建工厂
                new ThreadPoolExecutor.AbortPolicy());//拒绝策略
        for (int i = 0; i <10 ; i++) {
            Future<Integer> future=threadService.submit(new MyTask());
            //会阻塞直到拿到数据
            Integer jj=future.get();
            System.out.println(jj);
        }

        System.out.println("程序over");


    }
}

class MyTask implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        System.out.println("子线程正在运行中");
        Thread.sleep(3000);
        int sum=0;
        for (int i = 0; i < 100; i++) {
            sum+=i;
        }
        return sum;
    }
}