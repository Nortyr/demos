package com.pig.demos.threadPool;

import java.util.concurrent.*;

public class ThreadPoolDemo3 {
    public static void main(String[] args) throws InterruptedException {

        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(5, 10, 5000, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<>(1000),new ThreadPoolExecutor.DiscardOldestPolicy());
        for (int i = 0; i < 100000; i++) {
            threadPoolExecutor.execute(()->{
                try {
                    Thread.sleep(1);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });

        }
       while (true){
           synchronized (ThreadPoolDemo3.class){
               int queueSize=threadPoolExecutor.getQueue().size();
               int activeCount=threadPoolExecutor.getActiveCount();
               int poolSize=threadPoolExecutor.getPoolSize();
               System.out.println("queueSize-----"+queueSize);
               System.out.println("activeCount-----"+activeCount);
               System.out.println("poolSize------"+poolSize);
//               poolSizeThread.sleep();
           }

        }
    }
}
