package com.pig.demos.reflect;

import com.pig.demos.dynamicProxy.Test;

import java.lang.reflect.Method;

/**
 * created by MyPC on 2020/7/14
 */
public class ReflectDemo {
    public static void main(String[] args) throws NoSuchMethodException {
        Class a = Test.class;
        Method m = a.getMethod("sayHello");
        System.out.println(m.getDeclaringClass().getName());
        System.out.println(m.isDefault());
    }
}
