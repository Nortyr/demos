package com.pig.demos.intercepter;

import com.pig.demos.intercepter.MyHanderIntercepter2;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 可添加多个
        registry.addInterceptor(new MyHanderIntercepter2()).addPathPatterns("/**");
    }
}
