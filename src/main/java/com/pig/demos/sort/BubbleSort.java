package com.pig.demos.sort;

/**
 * 冒泡排序 o(n2)
 * 第一次把最大/最小第一个移动到第一个或者最后一个
 * 第二次把第二大/小移动到第一个或者最后一个
 */
public class BubbleSort {
    public static void main(String[] args) {
//        bubbleSort1();
        bubbleSort3();
    }

    private static void bubbleSort3() {
        int[] arr = new int[]{6, 2, 22, 45, 1, 6, 8, 200, 56, 111};
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
        ArrayPrint.printArr(arr);

    }

    public static void bubbleSort2() {
        int[] arr = new int[]{6, 2, 22, 45, 1, 6, 8, 200, 56, 111};
        for (int i = 0; i < arr.length; i++) {
            for (int j = i; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }

        }
        ArrayPrint.printArr(arr);
    }

    public static void bubbleSort1() {
        int[] arr = new int[]{6, 2, 22, 45, 1, 6, 8, 200, 56, 111};
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
        ArrayPrint.printArr(arr);
    }


}
