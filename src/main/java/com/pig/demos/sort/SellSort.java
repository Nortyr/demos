package com.pig.demos.sort;

/**
 * 分成多层次的插入排序
 */
public class SellSort {
    public static void main(String[] args) {
        day3();
    }

    private static void day3() {
        int[] arr = {49, 38, 65, 97, 76, 13, 27, 49, 78, 34, 12, 64, 1};
        int arrLength = arr.length;
        while (true) {
            arrLength /= 2;
            for (int i = 0; i < arr.length; i++) {
                for (int j = i + arrLength; j < arrLength; j += arrLength) {
                    int currentData = arr[i];
                    int currentKey = j - arrLength;
                    while (currentKey >= 0 && arr[currentKey] < currentData) {
                        arr[currentKey + arrLength] = arr[currentKey];
                        currentKey -= arrLength;
                    }
                    arr[currentKey + arrLength] = currentData;
                }
            }
            if (arrLength == 1) {
                break;
            }
        }
        ArrayPrint.printArr(arr);
    }

    private static void day2() {
        int[] arr = {49, 38, 65, 97, 76, 13, 27, 49, 78, 34, 12, 64, 1};
        int arrLength = arr.length;
        while (true) {
            arrLength /= 2;
            for (int i = 0; i < arrLength; i++) {
                for (int j = i + arrLength; j < arr.length; j += arrLength) {
                    int currentData = arr[j];
                    int preKey = j - arrLength;
                    while (preKey >= 0 && arr[preKey] > currentData) {
                        arr[preKey + arrLength] = arr[preKey];
                        preKey -= arrLength;
                    }
                    arr[preKey + arrLength] = currentData;


                }

            }
            if (arrLength == 1) {
                break;
            }
        }
        ArrayPrint.printArr(arr);
    }

    private static void day1() {
        int[] array = {49, 38, 65, 97, 76, 13, 27, 49, 78, 34, 12, 64, 1};
        int arrLength = array.length;
        while (true) {
            arrLength /= 2;
            for (int i = 0; i < arrLength; i++) {
                for (int j = i + arrLength; j < array.length; j += arrLength) {
                    int temp = array[j];
                    //后面一位与前面一位比较
                    int k = j - arrLength;
                    while (k >= 0 && array[k] > temp) {
                        array[k + arrLength] = array[k];
                        k -= arrLength;
                    }
                    array[k + arrLength] = temp;
                }
            }
            if (arrLength == 1) {
                break;
            }
        }
        ArrayPrint.printArr(array);
    }
}
