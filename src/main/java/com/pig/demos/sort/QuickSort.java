package com.pig.demos.sort;

import java.util.ArrayList;

/**
 * 递归，以一个数为基准，大的或者小的放到一边
 */
public class QuickSort {
    public static void main(String[] args) {
        int[] arr = new int[]{6, 2, 22, 45, 1, 6, 8, 200, 56, 111};
        quickSort(arr, 0, arr.length - 1);
        ArrayPrint.printArr(arr);
    }

    private static void quickSort(int[] arr, int left, int right) {
        int i = left;
        int j = right;
        int tmp = arr[i];
        while (i < j) {
            while (i < j && arr[j] > tmp) {
                j--;
            }
            while (i < j && arr[i] < tmp) {
                i++;
            }
            if (arr[i] == arr[j] && i < j) {
                i++;
            } else {
                int flag = arr[i];
                arr[i] = arr[j];
                arr[j] = flag;
            }
        }
        if (i - 1 > left) {
            quickSort(arr, left, i - 1);
        }
        if (j + 1 < right) {
            quickSort(arr, j + 1, left);
        }

    }
}
