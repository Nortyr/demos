package com.pig.demos.sort;

/**
 * @param
 * @author 陈晖
 * @description :插入排序，
 * 从后往前插入，前面的依次向后移动一位
 * @date 19:01 2020/8/4
 * @return
 */
public class InsertionSort {
    public static void main(String[] args) {
        day2();
    }

    private static void day2() {
        int[] arr = new int[]{6, 2, 22, 45, 1, 6, 8, 200, 56, 111};
        int currentData = 0;
        int preIndex = 0;
        for (int i = 1; i < arr.length; i++) {
            preIndex = i - 1;
            currentData = arr[i];
            while (preIndex >= 0 && arr[preIndex] > currentData) {
                arr[preIndex + 1] = arr[preIndex];
                preIndex--;
            }
            arr[preIndex + 1] = currentData;
        }
        ArrayPrint.printArr(arr);
    }

    private static void day1() {
        int[] arr = new int[]{6, 2, 22, 45, 1, 6, 8, 200, 56, 111};
        int preIndex = 0;
        int current = 0;
        for (int i = 1; i < arr.length; i++) {
            preIndex = i - 1;
            current = arr[i];
            while (preIndex >= 0 && arr[preIndex] > current) {
                arr[preIndex + 1] = arr[preIndex];
                preIndex--;
            }
            arr[preIndex + 1] = current;
        }
        ArrayPrint.printArr(arr);
    }
}
