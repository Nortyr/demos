package com.pig.demos.sort;

/**
 * 归并排序
 * 2个2个比较，再4个4个比较
 */
public class MergeSort {
    public static void main(String[] args) {
        int[] arr = new int[]{6, 2, 22, 45, 1, 6, 8, 200, 56, 111};
        int[] newArr = mergeSort(arr, 0, arr.length - 1);
        ArrayPrint.printArr(newArr);
    }

    private static int[] mergeSort(int[] arr, int start, int end) {
        if (start == end) {
            return new int[]{arr[start]};
        }
        int mid = start + (end - start) / 2;
        int[] leftArr = mergeSort(arr, start, mid);
        int[] rightArr = mergeSort(arr, mid + 1, end);
        int[] newNum = new int[leftArr.length + rightArr.length];
        int i = 0, m = 0, j = 0;
        while (i < leftArr.length && j < rightArr.length) {
            newNum[m++] = leftArr[i] < rightArr[j] ? leftArr[i++] : rightArr[j++];
        }
        //剩下来的一般就是大的且有顺序的，就依次放到后面
        while (i < leftArr.length) {
            newNum[m++] = leftArr[i++];
        }
        while (j < rightArr.length) {
            newNum[m++] = rightArr[j++];
        }
        return newNum;
    }
}
