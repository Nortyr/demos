package com.pig.demos.sort;

/**
 * 选择排序
 * 第一次找出最小/大的放到第一个
 * 第二次找出第二小/大的放到第二个
 */
public class SelectionSort {
    public static void main(String[] args) {
        selectionSort3();
    }

    private static void selectionSort3() {
        int[] arr = new int[]{6, 2, 22, 45, 1, 6, 8, 200, 56, 111};
        for (int i = 0; i < arr.length; i++) {
            int temp = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[temp] > arr[j]) {
                    temp = j;
                }
            }
            int temps = arr[i];
            arr[i] = arr[temp];
            arr[temp] = temps;
        }
        ArrayPrint.printArr(arr);
    }

    public static void selectionSort2() {
        int[] arr = new int[]{6, 2, 22, 45, 1, 6, 8, 200, 56, 111};
        for (int i = 0; i < arr.length; i++) {
            int temp = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[temp] > arr[j]) {
                    temp = j;
                }
            }
            int temps = arr[i];
            arr[i] = arr[temp];
            arr[temp] = temps;
        }
        ArrayPrint.printArr(arr);
    }

    public static void selectionSort1() {
        int[] arr = new int[]{6, 2, 22, 45, 1, 6, 8, 200, 56, 111};
        for (int i = 0; i < arr.length; i++) {
            int minTemp = i;
            for (int j = minTemp + 1; j < arr.length; j++) {
                if (arr[minTemp] > arr[j]) {
                    //每次找出最小的和第一个换
                    minTemp = j;
                }
            }
            int temp = arr[minTemp];
            arr[minTemp] = arr[i];
            arr[i] = temp;
        }
        ArrayPrint.printArr(arr);
    }
}
