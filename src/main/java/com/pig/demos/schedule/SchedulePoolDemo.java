package com.pig.demos.schedule;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;

public class SchedulePoolDemo {
    public static void main(String[] args) {
        ScheduledExecutorService executor = new ScheduledThreadPoolExecutor(10);
        for (int i = 0; i < 10000; i++) {
            final int temp = i;
            // 表示3秒后一起执行
            executor.schedule(() -> System.out.println(Thread.currentThread().getName() + "，i:" + temp), 1, TimeUnit.SECONDS);
        }
        executor.shutdown();


    }
}
