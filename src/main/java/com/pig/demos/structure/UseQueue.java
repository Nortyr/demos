package com.pig.demos.structure;

import java.util.LinkedList;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class UseQueue {
    public static void main(String[] args) {
        ConcurrentLinkedQueue linkedBlockingQueue = new ConcurrentLinkedQueue();
//        LinkedList linkedList=new LinkedList();
        AtomicInteger integer = new AtomicInteger();
        TimeUnit unit;
        BlockingQueue workQueue;
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(10, 15, 500, TimeUnit.MILLISECONDS, new LinkedBlockingDeque<>());
        for (int i = 0; i < 100; i++) {
            threadPoolExecutor.execute(() -> {
                for (AtomicInteger j = new AtomicInteger(); j.get() < 1000; j.incrementAndGet()) {
                    linkedBlockingQueue.add(new Object());
                }
                integer.incrementAndGet();
            });
        }
        while (true) {
            if (integer.get() == 100) {
                break;
            }
        }
        System.out.println(linkedBlockingQueue.size());
    }
}
