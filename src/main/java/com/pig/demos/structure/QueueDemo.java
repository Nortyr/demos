package com.pig.demos.structure;

import java.util.PriorityQueue;

public class QueueDemo {
    /**
     * 先进先出
     * 不阻塞队列：PriorityQueue（维护了列表），ConcurrentLinkedQueue（线程安全）
     * 阻塞队列（线程执行操作阻塞）：
     * ArrayBlockingQueue ：一个由数组支持的有界队列。
     * LinkedBlockingQueue ：一个由链接节点支持的可选有界队列。
     * PriorityBlockingQueue ：一个由优先级堆支持的无界优先级队列。
     * DelayQueue ：一个由优先级堆支持的、基于时间的调度队列。
     * SynchronousQueue ：一个利用 BlockingQueue 接口的简单聚集（rendezvous）机制。
     *
     * @param args 　　 add        增加一个元索              如果队列已满，则抛出一个IIIegaISlabEepeplian异常
     *             　　remove     移除并返回队列头部的元素    如果队列为空，则抛出一个NoSuchElementException异常
     *             　　element    返回队列头部的元素         如果队列为空，则抛出一个NoSuchElementException异常
     *             　　offer      添加一个元素并返回true     如果队列已满，则返回false
     *             　　poll       移除并返问队列头部的元素    如果队列为空，则返回null
     *             　　peek       返回队列头部的元素         如果队列为空，则返回null
     *             　　put        添加一个元素              如果队列满，则阻塞
     *             　　take       移除并返回队列头部的元素    如果队列为空，则阻塞
     */
    public static void main(String[] args) {
        PriorityQueue priorityQueue = new PriorityQueue();
        priorityQueue.add(new Object());
    }
}
