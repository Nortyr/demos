package com.pig.demos.map;

import java.util.HashMap;
import java.util.Map;

/**
 * created by MyPC on 2020/7/14
 */
public class MapDemos {
    public static void main(String[] args) {
        Map map = new HashMap();
        Object o1 = map.computeIfAbsent(1, (key) -> "1");
        Object o2 = map.computeIfAbsent(1, (key) -> "1");
        Object o3 = map.computeIfAbsent(1, (key) -> "" + key);
        Object o4 = map.computeIfAbsent(1, (key) -> "2");

        System.out.println(map);
    }
}
