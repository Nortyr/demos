package com.pig.demos;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Configuration
public class MyConfiguration {

    @Bean(name = "mySelfCache")
    public Map<String, String> initVerifyCodeCache() {
        return new HashMap<>();
    }
}
