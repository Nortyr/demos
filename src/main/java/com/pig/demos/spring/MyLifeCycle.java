package com.pig.demos.spring;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

//@Component
public class MyLifeCycle implements BeanNameAware, BeanFactoryAware, ApplicationContextAware, InitializingBean, DisposableBean {
    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("-------BeanFactoryAware");
    }

    @Override
    public void setBeanName(String s) {
        System.out.println("-------BeanNameAware");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("-------destroy");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("-------InitializingBean");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("-------ApplicationCOntextAware");
    }

    @PostConstruct
    public void selfInitialize() {
        System.out.println("-------selfInit");
    }

    @PreDestroy
    public void selfDestroy() {
        System.out.println("-------selfDestory");
    }
}
