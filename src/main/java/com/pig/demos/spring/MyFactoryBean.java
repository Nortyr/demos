package com.pig.demos.spring;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;


/**
 * 向容器中注册2个Bean，一个是MyFactoryBean本身，一个是getObject返回的Bean
 */
@Component
public class MyFactoryBean implements FactoryBean {


    @Override
    public Object getObject() throws Exception {

        return new MyLifeCycle();
    }

    @Override
    public Class<?> getObjectType() {
        return MyLifeCycle.class;
    }
}
