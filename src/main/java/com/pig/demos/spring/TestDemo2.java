package com.pig.demos.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestDemo2 {
    @Autowired
    private ApplicationContext applicationContext;

    @GetMapping("/testFactoryBean")
    public String testFactoryBean() {

        Object a = applicationContext.getBean(MyLifeCycle.class);
        Object myFactoryBean = applicationContext.getBean("myFactoryBean");
        Object myFactoryBean2 = applicationContext.getBean("&myFactoryBean");
        System.out.println(a);
        System.out.println(myFactoryBean);
        System.out.println(myFactoryBean2);
        return "aaaaa";
    }
}
