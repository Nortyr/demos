package com.pig.demos.spring;

import com.pig.demos.DemosApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * 倒入bean
 * 1. 扫描
 * 2. import注册到bean
 * <p>
 * 帮助我们去管理对象的
 * 初始化
 * class->BeanDifinition---->BeanFactoryPostProcessor---->new 实例->BeanPostProcessor-->数据填充->aware方法->初始化->aop->单例池
 * 1.beanClass
 * 2.scope
 * BeanDifinition:封装了bean
 * BeanFactoryPostProcessor
 * BeanPostProcessor
 *
 * @Configuration,@ComponentScan BeanFactory创建bean获取bean
 * BeanFactoryProcessor后置处理器：操作bean工厂
 * <p>
 * 扫描到@Component去生成一个BeanDifinition放入BeanDifinitionMap中
 * BeanFactory组建完成
 * 执行BeanFactoryPostProcessor
 * <p>
 * FactoryBean
 * <p>
 * -->使用缓存，先存到缓存中
 */
public class SpringDemo {

    public static void main(String[] args) {
        //Bean又BeanFactory创建

    }
}
