package com.pig.demos.dynamicProxy;

import sun.misc.ProxyGenerator;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Proxy;

/**
 * created by MyPC on 2020/7/13
 */
public class dynamicProxy {
    public static void main(String[] args) {
        // 1、生成$Proxy0的class文件
        System.getProperties().put("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");
        /**
         * Proxy类中还有个将2~4步骤封装好的简便方法来创建动态代理对象，
         *其方法签名为：newProxyInstance(ClassLoader loader,Class<?>[] instance, InvocationHandler h)
         */
        Test test = (Test) Proxy.newProxyInstance(Test.class.getClassLoader(), // 加载接口的类加载器
                new Class[]{Test.class}, // 一组接口
                new MyInvocationHandler(new Test() {
                    @Override
                    public void sayHello() {
                        System.out.println(11111);
                    }
                })); // 自定义的InvocationHandler
        test.sayHello();
        byte[] bytes = ProxyGenerator.generateProxyClass("$Proxy", new Class[]{test.getClass()});
        try (
                FileOutputStream fos = new FileOutputStream(new File("D:/$Proxy.class"))
        ) {
            fos.write(bytes);
            fos.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
