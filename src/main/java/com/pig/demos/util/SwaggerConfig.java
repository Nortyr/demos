package com.pig.demos.util;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Description: swagger配置
 * @Author: ZWT
 * @CreateDate: 2019/12/20 0020 上午 9:54
 * @UpdateDate: 2019/12/20 0020 上午 9:54
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    private final String SWAGGER2_API_GROUPNAME = "Demos";
    private final String SWAGGER2_API_BASEPACKAGE = "com.pig.demos";
    private final String SWAGGER2_API_TITLE = "Demos";
    private final String SWAGGER2_API_DESCRIPTION = "Demos";
    private final String SWAGGER2_API_URL = "Demos";
    private final String SWAGGER2_API_VERSION = "1.0";

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(SWAGGER2_API_GROUPNAME)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage(SWAGGER2_API_BASEPACKAGE))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(SWAGGER2_API_TITLE)
                .description(SWAGGER2_API_DESCRIPTION)
                .termsOfServiceUrl(SWAGGER2_API_URL)
                .version(SWAGGER2_API_VERSION)
                .build();
    }

}
